#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/spi.h>

#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "ili9341.h"

/* Backlight (PB_1) */
#define ILI9341_BACKLIGHT_RCC RCC_GPIOB
#define ILI9341_BACKLIGHT_PORT GPIOB
#define ILI9341_BACKLIGHT_PIN GPIO1

/* Chip select gpio (PB_7) */
#define ILI9341_SPI_CS_RCC RCC_GPIOD
#define ILI9341_SPI_CS_PORT GPIOD
#define ILI9341_SPI_CS_PIN GPIO7

/* Reset gpio (PD_13) */
#define ILI9341_RST_RCC RCC_GPIOD
#define ILI9341_RST_PORT GPIOD
#define ILI9341_RST_PIN GPIO13

/* Data/Command (dc) gpio (PD_5) */
#define ILI9341_DC_RCC RCC_GPIOD
#define ILI9341_DC_PORT GPIOD
#define ILI9341_DC_PIN GPIO5

#define ILI9341_SCREEN_COLUMN 240
#define ILI9341_SCREEN_ROW 320

static bool is_initialized = false;

typedef enum {
	E_ILI9341_MODE_COMMAND = 0,
	E_ILI9341_MODE_DATA = 1,
} E_ili9341_display_mode;

static int _change_display_mode(E_ili9341_display_mode new_mode)
{
	switch(new_mode)
	{
		case E_ILI9341_MODE_COMMAND:
			gpio_clear(ILI9341_DC_PORT, ILI9341_DC_PIN);
			break;
		case E_ILI9341_MODE_DATA:
			gpio_set(ILI9341_DC_PORT, ILI9341_DC_PIN);
			break;
		default:
			/*error*/
			return -1;
			break;
	}

	return 0;
}

static void _reset_display(void)
{
	/* Reset the display, reset is active low */
	gpio_clear(ILI9341_RST_PORT, ILI9341_RST_PIN);
	vTaskDelay(100);
	gpio_set(ILI9341_RST_PORT, ILI9341_RST_PIN);
	vTaskDelay(100);

	return;
}

static void _spi_init(void)
{
	/* Enable the GPIO ports whose pins we are using */
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_SPI2);

	spi_reset(SPI2);
	
	/* Configure GPIO, CS=PB_12, MOSI=PB_15, SCK=PB_13, MISO=PB14*/

	/* Output GPIO */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, GPIO13 | GPIO14 | GPIO15);

	/* Set alternative function (spi) for GPIO */
	gpio_set_af(GPIOB, GPIO_AF5, /*GPIO12 |*/ GPIO13 | GPIO14 | GPIO15);

	/* SCK + MOSI -> 25MHZ */
	gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO13 | GPIO15);

	/* Explicitly disable I2S in favour of SPI operation */
	SPI2_I2SCFGR = 0;

	spi_init_master(SPI2, SPI_CR1_BAUDRATE_FPCLK_DIV_2, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);

	spi_enable(SPI2);

	return;
}

static int _send_cmd(uint8_t cmd)
{
	/* Select CS */
	gpio_clear(GPIOB, GPIO12);

	/* Send command */
	_change_display_mode(E_ILI9341_MODE_COMMAND);
	spi_send(SPI2, cmd);
	(void)spi_read(SPI2);

	/* Clear CS */
	gpio_set(GPIOB, GPIO12);

	return 0;
}

static int _send_data8(uint8_t data)
{
	/* Select CS */
	gpio_clear(GPIOB, GPIO12);

	/* Send data */
	_change_display_mode(E_ILI9341_MODE_DATA);
	spi_send(SPI2, data);
	(void)spi_read(SPI2);

	/* Clear CS */
	gpio_set(GPIOB, GPIO12);

	return 0;
}

static int _send_data16(uint16_t data)
{
	_send_data8(data >> 8);
	_send_data8(data & 0xFF);
	return 0;
}

static void _send_ili9341_init_sequence(void)
{
	_send_cmd(0xEF);
	_send_data8(0x03);
	_send_data8(0x80);
	_send_data8(0x02);
	_send_cmd(0xCF);
	_send_data8(0x00);
	_send_data8(0xC1);
	_send_data8(0x30);
	_send_cmd(0xED);
	_send_data8(0x64);
	_send_data8(0x03);
	_send_data8(0x12);
	_send_data8(0x81);
	_send_cmd(0xE8);
	_send_data8(0x85);
	_send_data8(0x00);
	_send_data8(0x78);
	_send_cmd(0xCB);
	_send_data8(0x39);
	_send_data8(0x2C);
	_send_data8(0x00);
	_send_data8(0x34);
	_send_data8(0x02);
	_send_cmd(0xF7);
	_send_data8(0x20);
	_send_cmd(0xEA);
	_send_data8(0x00);
	_send_data8(0x00);

	// PWCTR1
	_send_cmd(0xC0);
	_send_data8(0x23);

	// PWCTR2
	_send_cmd(0xC1);
	_send_data8(0x10);

	// VMCTR1
	_send_cmd(0xC5);
	_send_data8(0x3E);
	_send_data8(0x28);

	// VMCTR2
	_send_cmd(0xC7);
	_send_data8(0x86);

	// MADCTL
	_send_cmd(0x36);
	_send_data8(0x48);

	// VSCRSADD
	_send_cmd(0x37);
	_send_data8(0x00);

	// PIXFMT
	_send_cmd(0x3A);
	_send_data8(0x55);

	// FRMCTR1
	_send_cmd(0xB1);
	_send_data8(0x00);
	_send_data8(0x18);

	// DFUNCTR
	_send_cmd(0xB6);
	_send_data8(0x08);
	_send_data8(0x82);
	_send_data8(0x27);
	_send_cmd(0xF2);
	_send_data8(0x00);

	// GAMMASET
	_send_cmd(0x26);
	_send_data8(0x01);

	// (Actual gamma settings)
	_send_cmd(0xE0);
	_send_data8(0x0F);
	_send_data8(0x31);
	_send_data8(0x2B);
	_send_data8(0x0C);
	_send_data8(0x0E);
	_send_data8(0x08);
	_send_data8(0x4E);
	_send_data8(0xF1);
	_send_data8(0x37);
	_send_data8(0x07);
	_send_data8(0x10);
	_send_data8(0x03);
	_send_data8(0x0E);
	_send_data8(0x09);
	_send_data8(0x00);
	_send_cmd(0xE1);
	_send_data8(0x00);
	_send_data8(0x0E);
	_send_data8(0x14);
	_send_data8(0x03);
	_send_data8(0x11);
	_send_data8(0x07);
	_send_data8(0x31);
	_send_data8(0xC1);
	_send_data8(0x48);
	_send_data8(0x08);
	_send_data8(0x0F);
	_send_data8(0x0C);
	_send_data8(0x31);
	_send_data8(0x36);
	_send_data8(0x0F);

	// Exit sleep mode.
	_send_cmd(0x11);

	vTaskDelay(200);

	// Display on.
	_send_cmd(0x29);
	vTaskDelay(200);

	// 'Normal' display mode.
	_send_cmd(0x13);

	return;
}

static void _init_gpio(void)
{
	/* Init blacklight pin */
	rcc_periph_clock_enable(ILI9341_BACKLIGHT_RCC);
	gpio_mode_setup(ILI9341_BACKLIGHT_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_BACKLIGHT_PIN);

	/* Init reset pin */
	rcc_periph_clock_enable(ILI9341_RST_RCC);
	gpio_mode_setup(ILI9341_RST_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_RST_PIN);

	/* init DC gpio (data/command mode pin)*/
	rcc_periph_clock_enable(ILI9341_DC_RCC);
	gpio_mode_setup(ILI9341_DC_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ILI9341_DC_PIN);
	
	/* init chip select gpio and set it to high (deselect) */
	gpio_set(GPIOB, GPIO12);
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO12);
	gpio_set(GPIOB, GPIO12);

	return; 
}

/* Public function */
int ili9341_fill_screen(uint16_t color)
{
	int ret = 0;

	if(is_initialized == false)
	{
		printf("ili9341 drivers is NOT initialized\n");
		return -1;
	}

	// Set column range.
	_send_cmd(0x2A);
	_send_data16(0x0000);
	_send_data16(ILI9341_SCREEN_COLUMN - 1);

	// Set row range.
	_send_cmd(0x2B);
	_send_data16(0x0000);
	_send_data16(ILI9341_SCREEN_ROW - 1);

	// Set 'write to RAM'
	_send_cmd(0x2C);

	// Write 320 * 240 pixels.
	for (uint32_t pixel_index = 0; pixel_index < (320*240); ++pixel_index)
	{
		_send_data16(color);
	}

	return 0;
}

int ili9341_set_pixel(uint16_t row, uint16_t column, uint16_t color)
{
	int ret = 0;

	if(is_initialized == false)
	{
		printf("ili9341 drivers is NOT initialized\n");
		return -1;
	}

	if(row > ILI9341_SCREEN_ROW)
	{
		printf("ili9341 row is too big, actual: %d, max: %d\n", row, ILI9341_SCREEN_ROW);
		return -2;
	}

	if(column > ILI9341_SCREEN_COLUMN)
	{
		printf("ili9341 column is too big, actual: %d, max: %d\n", column, ILI9341_SCREEN_COLUMN);
		return -3;
	}
	// Set column
	_send_cmd(0x2A);
	_send_data16(column);
	_send_data16(ILI9341_SCREEN_COLUMN - 1);

	// Set row
	_send_cmd(0x2B);
	_send_data16(row);
	_send_data16(ILI9341_SCREEN_ROW - 1);

	// Write pixel color
	_send_cmd(0x2C);
	_send_data16(color);

	return 0;
}

int ili9341_init(void)
{
	int ret = 0;

	if(is_initialized == true)
	{
		printf("ili9341 drivers is already initialized\n");
		return -1;
	}

	/* Init everything needed for the communication */
	_init_gpio();
	_spi_init();

	/* Reset and configure the display to be used */
	_reset_display();
	_send_ili9341_init_sequence();

	is_initialized = true;

	return 0;
}
