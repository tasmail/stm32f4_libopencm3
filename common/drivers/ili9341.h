#ifndef _ILI9341_DRIVER_H_
#define _ILI9341_DRIVER_H_

#define ILI9341_RGB565(red, green, blue) (((red & 0x1F) << 11) | ((green & 0x3F) << 5) | (blue & 0x1F))

int ili9341_init(void);
int ili9341_fill_screen(uint16_t color);
int ili9341_set_pixel(uint16_t row, uint16_t column, uint16_t color);

#endif /*_ILI9341_DRIVER_H_*/
